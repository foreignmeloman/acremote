# acremote
[![Build Status](https://travis-ci.org/foreignmeloman/acremote.svg?branch=master)](https://travis-ci.org/foreignmeloman/acremote)

## Installation
Requirements:
  - [Python](https://www.python.org/) 3.5+
  - [pigpio](https://github.com/joan2937/pigpio) V71+
  - root privilege to launch

```sh
cd acremote
pip3 install .
```

### Borrowed code
  - [ir-slinger.h](https://github.com/bschwind/ir-slinger)
